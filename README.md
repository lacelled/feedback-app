
## Feedback Application

Welcome to my repository for my feedback application. This was a project for school that I never deployed on an app store or online. 

It is a single html page using jQuery mobile application where you can leave reviews about places you have gone.

---

## Installation

**Prerequisites**

You need to have the following installed on configured

1. An IDE(Integrated Development Enviroment) such as Visual Studio Code.
2. A web browser, to view the application.

**Installion Steps**

1. Download the files / repository.
2. Open the folder with Visual Studio Code.
3. Run the application.

---

## Using the Application

**Adding/Editing a Review**

1. Fill/Edit out the place, date and details.
2. Save.
3. You will now see it under **Reviews** on the navigation bar.

**Viewing Reviews**

1. Navigate to the **Reviews** Tab.
2. Look at all reviews.
3. Click on a review you want more details of.

---

