/**
 *  File Name: DLfeedbackDAL.js
 *  Purpose: ##
 *
 *  Revision History: 2/28/2020 Dylan lacelle created
 *                    4/08/2020 Updated
 */
var DLReview = {
    insert: function(options, callback){
        function txFunction(tx){
            var sql = "INSERT INTO review(businessName, typeId, reviewerEmail, reviewerComments, reviewDate," +
                " hasRating, rating1, rating2, rating3) values(?,?,?,?,?,?,?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function(options, callback){
        function txFunction(tx){
            var sql = "SELECT * FROM review;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function(options, callback){
        function txFunction(tx){
            var sql = "SELECT * FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function(options, callback){
        function txFunction(tx){
            var sql = "UPDATE review SET businessName=?, typeId=?, reviewerEmail=?, reviewerComments=?, reviewDate=?," +
                " hasRating=?, rating1=?, rating2=?, rating3=? WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function(options, callback){
        function txFunction(tx){
            var sql = "DELETE FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};
var DLType = {
    selectAll: function(options, callback){
        function txFunction(tx){
            var sql = "SELECT * FROM type;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};