/**
 *  File Name: DLglobal.js
 *  Purpose: hold global methods and handlers
 *
 *  Revision History: 2/28/2020 Dylan lacelle created
 *                    4/08/2020 Updated
 */

function btnAddRating_click() {
    if(this.checked) {
        $("#DLRatingDiv").show();
    }
    else {
        $("#DLRatingDiv").hide();
    }
}
function btnEditRating_click() {
    if(this.checked) {
        $("#DLEditRatingDiv").show();
    }
    else {
        $("#DLEditRatingDiv").hide();
    }
}

function calculateOverallRating() {
    let a = $("#DLServiceRating").val();
    let b = $("#DLFoodRating").val();
    let c = $("#DLValueRating").val();

    let total = Math.round(((Number(a) + Number(b) + Number(c)) * 100) / 15);
    $("#DLRating").val(total + "%");
}

function DLAddFeedbackPage_show() {
    showFeedbackPage();
}
function DLAddNewFeedback_click() {
    DLaddFeedback();
}
function DLViewFeedbackPage_show() {
    DLgetReviews();
}
function DLEditFeedbackPage_show() {
    DLshowCurrentReview();
}

function DLUpdateFeedback_click() {
    DLUpdateFeedback();
}

function DLDeleteFeedback_click() {
    DLDeleteFeedback();
}

function DLClearDatabase_click() {
    DLClearDatabase();
}
function init() {
    console.info("DOM is ready");
    $("#DLAddRating").on("change", btnAddRating_click);
    $("#DLEditAddRating").on("change", btnEditRating_click);
    $("#DLServiceRating").on("change", calculateOverallRating);
    $("#DLFoodRating").on("change", calculateOverallRating);
    $("#DLValueRating").on("change", calculateOverallRating);
    $("#DLSubmit").on("click", DLAddNewFeedback_click);
    $("#DLUpdate").on("click", DLUpdateFeedback_click);
    $("#DLDelete").on("click", DLDeleteFeedback_click);
    $("#DLDefaultEmail").on("click", validate_DefaultEmail);
    $("#DLSaveDefaults").on("click", saveDefaultEmail);
    $("#DLAddFeedbackPage").on("pageshow", DLAddFeedbackPage_show);
    $("#DLViewFeedbackPage").on("pageshow", DLViewFeedbackPage_show);
    $("#DLEditFeedbackPage").on("pageshow", DLEditFeedbackPage_show);
    $("#DLClearDatabase").on("click", DLClearDatabase_click);
}

function initDB() {
    console.info("Creating database...");
    try {
        DB.DLCreateDatabase();
        if (db) {
            console.info("Creating tables...");
            DB.DLCreateTables();
        } else {
            console.error("Error: cannot create tables: DB not available!");
        }
    } catch (e) {
        console.error("Error: (Fatal) Error in initDB(). Can not proceed");
    }
}

$(document).ready(function() {
    init();
    initDB();
})