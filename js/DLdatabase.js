/**
 *  File Name: DLdatabase.js
 *  Purpose: ##
 *
 *  Revision History: 2/28/2020 Dylan lacelle created
 *                    4/08/2020 Updated
 */
var db;

function errorHandler(tx, error) {
    console.error("SQL error: " + tx + " (" + error.code + ") --");
}

var DB = {
    DLCreateDatabase: () => {
        var shortName = "DLFeedback";
        var version = "1.0";
        var displayName = "DB for DLFeedbackA3";
        var dbSize = 2 * 1084 * 1024;
        console.info("Creating database...");

        function dbCreateSuccess() {
            console.info("Success: Database created.");
        }
        db = openDatabase(shortName, version, displayName, dbSize, dbCreateSuccess);
    },
    DLCreateTables: () => {
        function txFunction(tx) {
            var sql = [
                "DROP TABLE IF EXISTS type;",
                "CREATE TABLE IF NOT EXISTS type( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,name VARCHAR(20) NOT NULL);",
                "INSERT INTO type (name) VALUES ('Canadian');",
                "INSERT INTO type (name) VALUES ('Asian');",
                "INSERT INTO type (name) VALUES ('Other');",
                "CREATE TABLE IF NOT EXISTS review( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "businessName VARCHAR(30) NOT NULL," +
                "typeId INTEGER NOT NULL," +
                "reviewerEmail VARCHAR(30)," +
                "reviewerComments TEXT," +
                "reviewDate DATE," +
                "hasRating VARCHAR(10)," +
                "rating1 INTEGER," +
                "rating2 INTEGER," +
                "rating3 INTEGER," +
                "FOREIGN KEY(typeID) REFERENCES type(id));"
            ];
            for (let i = 0; i < sql.length; i++) {
                var options = [];
                function successCallback() {
                    console.info("Success: Table created successfully");
                }
                tx.executeSql(sql[i], options, successCallback, errorHandler);
            }
        }
        function successTransaction() {
            console.info("Success: transaction successful");
        }
        db.transaction(txFunction,errorHandler,successTransaction);
    },
    DLDropTables: () => {
        function txFunction(tx) {
            var sql = ["DROP TABLE IF EXISTS review;", " DROP TABLE IF EXISTS type;"];
            var options = [];
            for (let i = 0; i < sql.length; i++) {
                tx.executeSql(sql[i], options, successCallback, errorHandler);
            }
            function successCallback() {
                console.info("Success: Tables dropped successfully");
            }

        }
        function successTransaction() {
            console.info("Success: transaction successful");
        }
        db.transaction(txFunction,errorHandler,successTransaction);
    }
}
