/**
 *  File Name: DLutil.js
 *  Purpose: hold validation methods
 *
 *  Revision History: 2/28/2020 Dylan lacelle created
 */

function validate_DLAddForm() {
    var form = $("#DLAddForm");
    form.validate({
       rules:{
           DLBusinessName:{
               required: true,
               rangelength: [2,20]
           },
           DLReviewerEmail:{
               required: true,
               email: true,
               emailcheck: true
           },
           DLReviewerDate: {
               required: true,
               date: true
           },
           DLFoodRating: {
               required: true,
               range: [0,5]
           },
           DLValueRating: {
               required: true,
               range: [0,5]
           },
           DLServiceRating: {
               required: true,
               range: [0,5]
           }
       },
        messages:{
            DLBusinessName:{
                required: "You must provide a business name.",
                rangelength: "Length must be 2-20 characters long."
            },
            DLReviewerEmail:{
                required: "Email must be provided.",
                email: "Please use a valid email.",
                emailcheck: "Please use a valid email."
            },
            DLReviewerDate: {
                required: "Date must be provided.",
                date: "Please enter a valid date."
            },
            DLFoodRating: {
                required: "Food rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            },
            DLValueRating: {
                required: "Value rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            },
            DLServiceRating: {
                required: "Service rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            }
        }
    });
    return form.valid();
}

function validate_DLEditForm() {
    var form = $("#DLEditForm");
    form.validate({
        rules:{
            DLEditBusinessName:{
                required: true,
                rangelength: [2,20]
            },
            DLEditReviewerEmail:{
                required: true,
                email: true,
                emailcheck: true
            },
            DLEditReviewerDate: {
                required: true,
                date: true
            },
            DLEditFoodRating: {
                required: true,
                range: [0,5]
            },
            DLEditValueRating: {
                required: true,
                range: [0,5]
            },
            DLEditServiceRating: {
                required: true,
                range: [0,5]
            }
        },
        messages:{
            DLEditBusinessName:{
                required: "You must provide a business name.",
                rangelength: "Length must be 2-20 characters long."
            },
            DLEditReviewerEmail:{
                required: "Email must be provided.",
                email: "Please use a valid email.",
                emailcheck: "Please use a valid email."
            },
            DLEditReviewerDate: {
                required: "Date must be provided.",
                date: "Please enter a valid date."
            },
            DLEditFoodRating: {
                required: "Food rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            },
            DLEditValueRating: {
                required: "Value rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            },
            DLEditServiceRating: {
                required: "Service rating must be provided if you want to add rating.",
                range: "Please select a rating from 0 to 5."
            }
        }
    });
    return form.valid();
}

function validate_DefaultEmail() {
    var form = $("#DLDefaultEmail");
    form.validate({
        rules:{
            DLDefaultReviewerEmail:{
                required: true,
                emailcheck: true
            }
        },
        messages:{
            DLDefaultReviewerEmail:{
                required: "You must provide an email.",
                emailcheck: "Please provide a valid email."
            }
        }
    });
    return form.valid();
}

jQuery.validator.addMethod("emailcheck",
    (value, element) => {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(value);
    }, "Email validator");

function saveDefaultEmail() {
    localStorage.setItem("defaultEmail", $("#DLDefaultReviewerEmail").val());
    alert("Default reviewer email " +  $("#DLDefaultReviewerEmail").val() + " successfully saved.");
}

function CalculateRating(row) {
    let a = row['rating1'];
    let b = row['rating2'];
    let c = row['rating3'];

    return Math.round(((Number(a) + Number(b) + Number(c)) * 100) / 15);
}