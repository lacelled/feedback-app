/**
 *  File Name: DLfacade.js
 *  Purpose: ##
 *
 *  Revision History: 2/28/2020 Dylan lacelle created
 *                    4/08/2020 Updated
 */

function showFeedbackPage() {
    // Load default email
    if (localStorage.getItem("emailAddress") != null) {
        $("#DLReviewerEmail").val(localStorage.getItem("emailAddress"));
    }
    //load strings to populate type dropdown
    DLupdateTypesDropdown();
}

function DLupdateTypesDropdown() {
    var options = [];

    function callback(tx, results) {
        $("#DLType").empty();
        for (let i = 0; i < results.rows.length; i++) {
            $("#DLType").append(`<option value="${i + 1}"> 
                                       ${results.rows.item(i).name} 
                                  </option>`);
        }
        //set default to other
        $("#DLType").val('3');

    }
    DLType.selectAll(options, callback);
}

function DLaddFeedback() {
    if(validate_DLAddForm()) {
        console.info("add form validation successful");
        var options;
        var businessName = $("#DLBusinessName").val();
        var typeId = $("#DLType").val();
        var email = $("#DLReviewerEmail").val();
        var comments = $("#DLComments").val();
        var date = $("#DLReviewDate").val();
        var hasRating;
        var foodRating = "";
        var serviceRating = "";
        var valueRating = "";

        if($("#DLAddRating").prop("checked")) {
            hasRating = "true";
            foodRating = $("#DLFoodRating").val();
            serviceRating = $("#DLServiceRating").val();
            valueRating = $("#DLValueRating").val();
        }
        else {
            hasRating = "false";
        }
        function callback() {
            alert("New Feedback Added");
        }
        options = [businessName, typeId, email, comments, date, hasRating, foodRating, serviceRating, valueRating];
        DLReview.insert(options, callback);
    }
    else {
        console.error("Validation failed");
    }
}

function DLgetReviews() {
    var options = [];
    function callback(tx, results){
        var htmlCode = "";

        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows.item(i);
            htmlCode += "<li><a data-role='button' data-row-id="+ row['id'] + " href='#'>" +
                "<h2>Business Name: "+ row['businessName'] + "</h2>" +
                "<p>Reviewer Email: "+ row['reviewerEmail'] + "</p>" +
                "<p>Comments: "+ row['reviewerComments'] + "</p>" +
                "<p>Overall Rating: "+ CalculateRating(row) + "</p>" +
                "</a></li>";
        }
        var reviewList = $("#DLReviewList");
        reviewList = reviewList.html(htmlCode);
        reviewList.listview("refresh");
        $("#DLReviewList a").on("click", clickHandler);
        function clickHandler() {
            var id = $(this).attr("data-row-id");
            localStorage.setItem("id", id);
            $(location).prop('href', '#DLEditFeedbackPage');
        }
    }
    DLReview.selectAll(options, callback);
}

function DLshowCurrentReview() {
    var id = localStorage.getItem("id");
    var options = [id];

    function callback(tx, results) {
        var row = results.rows.item(0);
        $("#DLEditBusinessName").val(row['businessName']);
        $("#DLEditType").val(row['typeId']);
        $("#DLEditReviewerEmail").val(row['reviewerEmail']);
        $("#DLEditComments").val(row['reviewerComments']);
        $("#DLEditReviewDate").val(row['reviewDate']);
        if(row['hasRating'] == 'true') {
            $("#DLEditAddRating").prop('checked', true);
            $("#DLEditFoodRating").val(row['rating1']);
            $("#DLEditServiceRating").val(row['rating2']);
            $("#DLEditValueRating").val(row['rating3']);
        }
        else {
            $("#DLEditAddRating").prop('checked', false);
        }
        $("#DLEditAddRating").checkboxradio("refresh");
    }
    DLReview.select(options, callback);
}

function DLUpdateFeedback() {
    if(validate_DLEditForm()) {
        var id = localStorage.getItem("id");
        var name = $("#DLEditBusinessName").val();
        var type = $("#DLEditType").val();
        var email = $("#DLEditReviewerEmail").val();
        var comments = $("#DLEditComments").val();
        var date = $("#DLEditReviewDate").val();
        var addRating = $("#DLEditAddRating").prop('checked');
        var foodRating = $("#DLEditFoodRating").val();
        var serviceRating = $("#DLEditServiceRating").val();
        var valueRating = $("#DLEditValueRating").val();
        function callback(tx, results) {
            alert("Feedback updated successfully.");
        }
        var options = [name, type, email, comments, date, addRating, foodRating, serviceRating, valueRating, id];
        DLReview.update(options, callback);
        $(location).prop('href', '#DLViewFeedbackPage');
    }
    else {
        console.error("Edit validation failed");
    }
}

function DLDeleteFeedback() {
    var id = localStorage.getItem("id");
    var options = [id];
    function callback() {
        alert("Feedback deleted successfully");
    }
    DLReview.delete(options,callback);
    $(location).prop('href', '#DLViewFeedbackPage');
}

function DLClearDatabase() {
    var result = confirm("Are you sure you want to clear the database?");
    try {
        if(result) {
            DB.DLDropTables();
            alert("Database cleared");
        }
    } catch (e) {
        alert(e);
    }
}